var modal = false;

jQuery(document).ready(function($) {

	// Create the media frame.
	var modal_imagen = wp.media({
		// Set the title of the modal.
		title: 'Imagen destacada',
		button: {
			text: 'Imagen destacada',
		},
		states : [
			new wp.media.controller.Library({
				title: 'Asignar Imagen',
				filterable :	'image',
				multiple: false,
			})
		]
	});
	
	//Al seleccionar una imagen
	modal_imagen.on( 'select', function() {

		var answer = false;
		var selection = modal_imagen.state().get('selection');

		selection.map( function( attachment ) {

			attachment = attachment.toJSON();

			if ( attachment.id ) {
				jQuery(modal).addClass('seleccionada');
				var th = attachment.sizes.full.url;
				if(attachment.sizes.thumbnail !== undefined) {
					th = attachment.sizes.thumbnail.url;
				}
				
				select_item(attachment.id, th);
			}
		} );

		//bmo_item(key, answer);
	});

	//Al hacer click en el item para agregar
	jQuery(document).on( 'click', '.modal .asignar, .modal .agregar, .modal img', function( event ) {
		modal_value = false;
		var parent = jQuery(this).parents('.modal');
		modal = jQuery(parent);
		
		event.preventDefault();
		
		modal_imagen.open();

	});
		
	//Al hacer click en eliminar
	jQuery('.modal .eliminar').on( 'click', function( event ) {
		modal_value = false;
		var parent = jQuery(this).parents('.modal');
		modal = jQuery(parent);
		
		jQuery(modal).removeClass('seleccionada');
		jQuery(modal).find('input[type=hidden]').val('');
		jQuery(modal).find('img').attr('src', '');
	});

});


function add_item(id, name, th, src) {
	var aux = modal.find('.repositorio').children().clone();
	
	jQuery(aux).find('input[data-ext=id]').val(id);
	jQuery(aux).find('input[data-ext=titulo]').val(name);
	jQuery(aux).find('input[data-ext=src]').val(src);
	jQuery(aux).find('input[data-ext=thumbnail]').val(th);
	jQuery(aux).find('span.title').html(name);
	jQuery(aux).find('img.imagen').attr('src', th);
	
	var lista = modal.find('.items');
	agregarElement(aux, lista);
}

function select_item(id, th) {
	jQuery(modal).find('input[type=hidden]').val(id);
	jQuery(modal).find('img').attr('src', th);
}
