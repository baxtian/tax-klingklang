jQuery(document).ready( function() {
	//Listas ordenables
	if(jQuery( ".listado.ordenable").length > 0)
		jQuery( ".listado.ordenable .items" ).sortable({ revert: true });
		
	//Eliminar elemento de la lista
	jQuery(document).on( 'click', '.listado input[type=button].eliminar', function() {
		jQuery(this).parent().remove();
	} );

	//Agregar elemento a la lista
	jQuery(document).on( 'click', '.invitacion.button, input[type=button].agregar', function() {
		var parent = jQuery(this).parents('.listado');
		parent = parent[0];
		var aux = jQuery(parent).find('.repositorio .item').clone();
		var lista = jQuery(parent).find('.items');
		aux.find('input[type=text]').val('');
		aux.find('select option').prop('selected', false);
		aux.appendTo( jQuery(lista) );
		
		/*jQuery(aux).find('input.sugerir').each( function() { 
			sugerencia(this);
			jQuery(this).focus();
		})*/
		
	} );

});
