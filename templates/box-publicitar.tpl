<div class='row'><label>¿Publicitar?</label> <input type="checkbox" name="publicitar" {{ function('checked', publicitar, true, false) }}></div>
<div class='row'>
	<label>Logo:</label>
	<div id="logo_container" class="modal{% if imagen %} seleccionada{% endif %}">
		<img class='definida' src='{{ imagen.src }}'>
		<span class='definida howto'>Haz clic en la imagen para editarla o actualizarla</span>
		<span class='definida eliminar'><a>Quitar el logo</a></span>
		<input type='hidden' id='img_id' name='img_id' value='{{ imagen.id }}'>
		<p class="add_logo hide-if-no-js asignar">
			<a href="#" data-choose="Asignar imagen publicitaria" data-update="Asignar imagen publicitaria" data-delete="Descartar imagen publicitaria" data-text="Descartar">Asignar imagen publicitaria</a>
		</p>
	</div>
</div>
