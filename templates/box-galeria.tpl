<div id="galeria_imagenes_container">
	<ul class="product_images">{% for imagen in imagenes  %}
		<li class="image" data-attachment_id="{{ imagen.id }}">
			{{ imagen.src }}
			<ul class="actions">
				<li><a href="#" class="delete tips" data-tip="Eliminar imagen">Eliminar</a></li>
			</ul>
		</li>{% endfor %}
	</ul>

	<input type="hidden" id="galeria_imagenes" name="galeria_imagenes" value="{{ galeria_imagenes }}" />
</div>
<p class="add_gallery_image hide-if-no-js">
	<a href="#" data-choose="Agregar Imágenes a la galería del producto" data-update="Agregar a galería" data-delete="Eliminar Imagen" data-text="Eliminar">Agregar galería de imágenes</a>
</p>
