<?php
/*
Plugin Name: Tax-KlingKlang
Version: 0.2
Description: Taxonomía de KlingKlang
Author: Juan Sebastián Echeverry
Text Domain: tax-klingklang

Copyright 2016 Juan Sebastián Echeverry (baxtian.echeverry@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Do not active this plugin if timber isn't installed
include_once(ABSPATH . 'wp-admin/includes/plugin.php');
if (!is_plugin_active('timber-library/timber.php')) {
    add_action('admin_notices', function () {
        echo '<div id="message" class="error fade"><p style="line-height: 150%">';
        _e('<strong>Tax-KlingKlang</strong> requiere el plugin <strong>Timber</strong>', 'hwic');
        echo '</p></div>';
    });
    return;
}

define("PLA_KLK_V", 0.03);

add_action('init', 'klk_taxonomy');
add_action('admin_init', 'klk_adminheader');

add_action('save_post_proyecto', 'klk_save_galeria');
add_action('save_post_product', 'klk_save_publicitar');
add_action('save_post_product', 'klk_save_colores');

add_action('save_post_post', 'klk_save_hashtag');
add_action('save_post_product', 'klk_save_hashtag');
add_action('save_post_proyecto', 'klk_save_hashtag');

// Función que declara los estilos y scripts para visualizar al
// administrar el sitio
function klk_adminheader()
{
    wp_register_style('modal', plugin_dir_url(__FILE__)."css/modal.css", array(), PLA_KLK_V);
    wp_register_style('klingklang', plugin_dir_url(__FILE__)."css/admin.css", array( 'modal' ), PLA_KLK_V);
    wp_enqueue_style('klingklang');

    wp_register_script('meta-boxes-product', plugin_dir_url(__FILE__)."scripts/meta-boxes-product.js", array('jquery', 'jquery-ui-draggable'), PLA_KLK_V);
    wp_register_script('modal', plugin_dir_url(__FILE__)."scripts/modal.js", array('jquery', 'jquery-ui-sortable'), PLA_KLK_V);
    wp_register_script('admin', plugin_dir_url(__FILE__)."scripts/admin.js", array('jquery', 'jquery-ui-sortable'), PLA_KLK_V);
}


// Función para agregar la taxonomía
function klk_taxonomy()
{

    //Registrar 'proyecto' como una nueva estructura de información
    $labels = array(
        'name' => 'Proyecto',
        'singular_name' => 'Proyecto',
        'add_new' => 'Agregar nuevo',
        'add_new_item' => 'Agregar nuevo Proyecto',
        'edit_item' => 'Editar Proyecto',
        'new_item' => 'Nuevo Proyecto',
        'view_item' => 'Ver Proyecto',
        'search_items' => 'Buscar Proyecto',
        'not_found' =>  'No se encontraron Proyectos',
        'not_found_in_trash' => 'No se encontraron Proyectos en la papelera',
        'parent_item_colon' => '',
        'menu_name' => 'Proyectos'
    );

    $args = array(
        'query_var' => true,
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'taxonomies' => array( 'category' ),
        'supports' => array('title', 'editor', 'excerpt'),
        'menu_icon' => 'dashicons-art'
    );
    register_post_type('proyecto', $args);

    //Registrar 'servicio' como una nueva estructura de información
    /*$labels = array(
        'name' => 'Servicio',
        'singular_name' => 'Servicio',
        'add_new' => 'Agregar nuevo',
        'add_new_item' => 'Agregar nuevo Servicio',
        'edit_item' => 'Editar Servicio',
        'new_item' => 'Nuevo Servicio',
        'view_item' => 'Ver Servicio',
        'search_items' => 'Buscar Servicio',
        'not_found' =>  'No se encontraron Servicios',
        'not_found_in_trash' => 'No se encontraron Servicios en la papelera',
        'parent_item_colon' => '',
        'menu_name' => 'Servicios'
    );

    $args = array(
        'query_var' => true,
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'taxonomies' => array( ),
        'supports' => array('title', 'editor', 'excerpt'),
        'menu_icon' => 'dashicons-businessman'
    );
    register_post_type('servicio',$args);*/

    // Agregar nueva taxonomía para 'materiales' no jerárquica y vincularla
    // con las estructura proyecto
    $labels = array(
        'name' => 'Materiales',
        'singular_name' => 'Material',
        'search_items' =>  'Buscar Materiales',
        'all_items' => 'Todos los Materiales',
        'edit_item' => 'Editar Material',
        'update_item' => 'Acualizar Material',
        'add_new_item' => 'Agregar nuevo Material',
        'new_item_name' => 'Nombre de nuevo Material',
        'menu_name' => 'Materiales',
        'popular_items' => 'Materiales Populares',
        'add_or_remove_items' => 'Agregar o eliminar Materiales',
        'parent_item' => null, 'parent_item_colon' => null,
        'choose_from_most_used' => 'Elegir entre los Materiales más usados',
        'separate_items_with_commas' => 'Separe Materiales con comas'
    );

    register_taxonomy('material', array('product'), array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
    ));

    // Agregar nueva taxonomía para 'técnicas' no jerárquica y vincularla
    // con la estructura proyecto
    $labels = array(
        'name' => 'Técnicas',
        'singular_name' => 'Técnica',
        'search_items' =>  'Buscar Técnicas',
        'all_items' => 'Todas los Técnicas',
        'edit_item' => 'Editar Técnica',
        'update_item' => 'Acualizar Técnica',
        'add_new_item' => 'Agregar nueva Técnica',
        'new_item_name' => 'Nombre de nueva Técnica',
        'menu_name' => 'Técnicas',
        'popular_items' => 'Técnicas Populares',
        'add_or_remove_items' => 'Agregar o eliminar Técnicas',
        'parent_item' => null, 'parent_item_colon' => null,
        'choose_from_most_used' => 'Elegir entre las Técnicas más usadas',
        'separate_items_with_commas' => 'Separe Técnicas con comas'
    );

    register_taxonomy('tecnica', array('product'), array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
    ));

    // Enlace a función que crea las cajas donde estarán las entradas de
    // las variables
    add_action('add_meta_boxes', 'klk_boxes');
}

// Función para crear las cajas deonde estarán las variables
function klk_boxes()
{

    //Campos especiales para las biticias destacadas
    add_meta_box(
        'galeria',
        'Galería',
        'klk_box_galeria',
        'proyecto',
        'side'
    );

    //Campos especiales para publicitar un producto
    add_meta_box(
        'publicitar',
        'Publicitar',
        'klk_box_publicitar',
        'product',
        'advanced'
    );

    //Campos especiales para colores un producto
    add_meta_box(
        'colores',
        'Colores',
        'klk_box_colores',
        'product',
        'side'
    );

    //Campos especiales para hashtag un post
    add_meta_box(
        'hashtag',
        'Hashtag',
        'klk_box_hashtag',
        'post',
        'advanced'
    );

    //Campos especiales para hashtag un producto
    add_meta_box(
        'hashtag',
        'Hashtag',
        'klk_box_hashtag',
        'product',
        'advanced'
    );

    //Campos especiales para hashtag un proyecto
    add_meta_box(
        'hashtag',
        'Hashtag',
        'klk_box_hashtag',
        'proyecto',
        'advanced'
    );
}

// Caja especial para noticias destacadas (página de inicio)
function klk_box_galeria()
{
    global $post;

    // Obtener las variables
    $galeria_imagenes = get_post_meta($post->ID, '_galeria_imagenes', true);

    $attachments = array_filter(explode(',', $galeria_imagenes));

    $imagenes = array();
    if (! empty($attachments)) {
        foreach ($attachments as $attachment_id) {
            $imagenes[] = array(
                'id' => $attachment_id,
                'src' => wp_get_attachment_image($attachment_id, 'thumbnail')
            );
        }
    } elseif (empty($attachments) && $thumbnail_id = get_post_meta($post->ID, '_thumbnail_id', true)) {
        $imagenes[] = array(
            'id' => $thumbnail_id,
            'src' => wp_get_attachment_image($thumbnail_id, 'thumbnail')
        );
        $galeria_imagenes = $thumbnail_id;
    }

    $args['galeria_imagenes'] = esc_attr($galeria_imagenes);
    $args['imagenes'] = $imagenes;

    wp_enqueue_script('meta-boxes-product');

    // Twig
    klk_render("box-galeria.tpl", $args);
}

// Caja especial para noticias destacadas (página de inicio)
function klk_box_publicitar()
{
    global $post;

    // Obtener las variables
    $publicitar = get_post_meta($post->ID, '_publicitar', true);
    $id_img = get_post_meta($post->ID, '_imagen_publicitaria', true);

    //Obtener datos de la imagen
    $imagen = false;
    if ($id_img) {
        $src = wp_get_attachment_image_src($id_img, 'thumbnail');
        $imagen = array(
            'id' => $id_img,
            'src' => $src[0]
        );
    }

    //Argumentos
    $args['publicitar'] = $publicitar;
    $args['imagen'] = $imagen;

    wp_enqueue_script('modal');

    // Twig
    klk_render("box-publicitar.tpl", $args);
}

// Caja especial para noticias destacadas (página de inicio)
function klk_box_hashtag()
{
    global $post;

    // Obtener las variables
    $hashtag = get_post_meta($post->ID, '_hashtag', true);
    $hashtag_link = get_post_meta($post->ID, '_hashtag_link', true);

    //Argumentos
    $args['hashtag'] = $hashtag;
    $args['hashtag_link'] = $hashtag_link;

    // Twig
    klk_render("box-hashtag.tpl", $args);
}

// Caja especial para colores
function klk_box_colores()
{
    global $post;

    // Obtener las variables
    $colores = get_post_meta($post->ID, '_colores', true);

    if (!is_array($colores) || count($colores) == 0) {
        $colores = array();
    }

    $args['colores'] = $colores;

    wp_enqueue_script('admin');

    // Twig
    klk_render("box-colores.tpl", $args);
}

// Función para guardar los datos de la galería
function klk_save_galeria($post_id)
{
    $attachment_ids = isset($_POST['galeria_imagenes']) ? array_filter(explode(',', sanitize_text_field($_POST['galeria_imagenes']))) : array();

    update_post_meta($post_id, '_galeria_imagenes', implode(',', $attachment_ids));

    //Obtener la primera imagen y guarlarla como thumbnail
    if (is_array($attachment_ids) && count($attachment_ids) > 0) {
        update_post_meta($post_id, '_thumbnail_id', $attachment_ids[0]);
    } else {
        delete_post_meta($post_id, '_thumbnail_id');
    }
}

// Función para guardar los datos de la galería
function klk_save_publicitar($post_id)
{
    $publicitar = isset($_POST['publicitar']) ? true : false;
    $id_img = ($publicitar) ? $_POST['img_id'] : false;

    update_post_meta($post_id, '_publicitar', $publicitar);
    update_post_meta($post_id, '_imagen_publicitaria', $id_img);
}

// Función para guardar los datos de hashtag
function klk_save_hashtag($post_id)
{
    $hashtag = (isset($_POST['hashtag'])) ? trim($_POST['hashtag']) : false;
    $hashtag_link = (isset($_POST['hashtag_link'])) ? esc_url(trim($_POST['hashtag_link'])) : false;

    update_post_meta($post_id, '_hashtag', $hashtag);
    update_post_meta($post_id, '_hashtag_link', $hashtag_link);
}

// Función para guardar los datos extra de los productos
function klk_save_colores($post_id)
{
    // Verificar que no sea una rutina de autoguardado.
    // Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
    // el proceso.
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    //Revisar que estemos editando
    if (!isset($_POST['action']) || $_POST['action']!= 'editpost') {
        return;
    }

    // Revisar permisos del usuario
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    // Obtener el valor de la variable desde el formulario
    $_colores = $_POST['colores'];
    $colores = array();

    if (isset($_POST['colores']) && is_array($_colores)) {
        foreach ($_colores as $item) {
            $colores[] = $item;
        }
    }

    //Eliminar el item del repositorio
    array_pop($colores);

    // Almacenar variables
    update_post_meta($post_id, '_colores', $colores);

    return;
}

function klk_render($tpl, $args, $echo = true)
{
    $context = Timber::get_context();
    $context['plugin'] = array( 'directory' => plugin_dir_url(__FILE__) );
    Timber::$dirname = 'templates';

    //Render
    if ($echo) {
        return Timber::render($tpl, array_merge($context, $args));
    }
    // o feed?
    return Timber::fetch($tpl, array_merge($context, $args));
}
